<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

//Show User profile
Route::get('/', 'HomeController@index')->name('home');


//Edit user profile, to add hobbies
Route::get('/edit', 'HobbiesController@index');
//Route::get('/insert', 'HobbiesController@insert')->name('hobbies.insert');
Route::post('/edit', 'HobbiesController@insert')->name('hobbies.insert');
Route::get('/del/{id}', 'HobbiesController@destroy');

