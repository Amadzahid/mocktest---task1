<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Hobbies extends Model
{
    protected $table = 'hobbies';
    protected $fillable = ['user_id','hname'];

    //Relationship b/w Hobbies and User
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
