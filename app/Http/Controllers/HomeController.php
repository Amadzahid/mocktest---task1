<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Hobbies;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //Home controller to fetch Login User Profile data.
        $currentuserid = Auth::user()->id;
        //$hobbies = Hobbies::all('user_id' => $currentuserid);
        //$hobbies = Hobbies::all();
        $hobbies = Hobbies::where("users_id", "=", $currentuserid)->get();

        //var_dump($hobbies); die;
        return view('home', array('user' => Auth::user(), 'hobbies' => $hobbies));
    }



}
