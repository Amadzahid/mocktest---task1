<?php

namespace App\Http\Controllers;

use App\Hobbies;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Redirect;

class HobbiesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }



    //Edit Profile to add hobbies

    function index()
    {
        $currentuserid = Auth::user()->id;
        //$hobbies = Hobbies::all('user_id' => $currentuserid);
        //$hobbies = Hobbies::all();
        $hobbies = Hobbies::where("users_id", "=", $currentuserid)->get();

        return view('profile.edit', array('user' => Auth::user(), 'hobbies' => $hobbies));
    }



    function insert(Request $request)
    {
        if($request->ajax())
        {
            $rules = array(
                'hname.*'  => 'required'
            );
            $error = Validator::make($request->all(), $rules);
            if($error->fails())
            {
                return response()->json([
                    'error'  => $error->errors()->all()
                ]);
            }

            $users_id = $request->users_id;
            $hname = $request->hname;
            //$insert_data[] = $users_id;
            for($count = 0; $count < count($hname); $count++)
            {

                $data = array(
                    'users_id' => $users_id,
                    'hname' => $hname[$count]
                );
                $insert_data[] = $data;
            }


            for ($c = 0; $c < count($insert_data); $c++){
                //print_r($insert_data[$c]['hname']);
                //Check If hobby name exist or not
                $hb = Hobbies::where('hname', '=', $insert_data[$c]['hname'])->count();
                if($hb == 1){

                    return response()->json([
                        'exist'  => "Hobby ".$insert_data[$c]['hname'] ." already exist"
                    ]);

                }else{
                    //Insert Hobbies
                    Hobbies::insert($insert_data);
                    $request->session()->flash('success', 'Hobby Added successfully.');
                    return response()->json([
                        'success'  => 'Hobby Added successfully.'
                    ]);
                }
            }


        }
    }


    public function destroy($id)
    {
        $hobbies = Hobbies::find($id);
        $hobbies->delete();

        // redirect
        \Session::flash('message', 'Successfully deleted Hobby!');
        return Redirect::to('edit');

    }



}
