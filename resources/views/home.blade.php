@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>

            <div class="col-xs-12">
                <br/>
                <div class="card">
                    <span class="" role="alert" id="result">
                                <!-- will be used to show any messages -->
                        @if(session('success'))
                            <div class="alert alert-success">
                                        <strong>{{ Session::get('success') }}</strong>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                        @endif
                    </span>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Hobbies</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">{!!Auth::user()->id !!}</th>
                            <td>{!!Auth::user()->name !!}</td>
                            <td>{!!Auth::user()->email !!}</td>
                            <td>
                                <ul>
                                    @foreach($hobbies as $hobby)
                                    <li>
                                        {{ $hobby->hname }}
                                    </li>
                                    @endforeach
                                </ul>
                            </td>
                            <td>
                                <a href="/edit"><i class="fa fa-plus" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>


            </div>

        </div>
    </div>
</div>
@endsection
