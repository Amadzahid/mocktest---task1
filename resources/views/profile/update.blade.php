@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Update Profile') }}</div>

                    <div class="card-body">
                        <form method="post" id="hobbies_form">
                            <span class="invalid-feedback" role="alert" id="result"></span>
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('name') is-invalid @enderror" name="name" value="{!!Auth::user()->name !!}" disabled required />

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{!!Auth::user()->email !!}" disabled required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="hname" class="col-md-4 col-form-label text-md-right">{{ __('Hobbies') }}</label>
                                <div class="col-md-6" id="hname">

                                    @foreach($hobbies as $hobby)
                                        <li>
                                            {{ $hobby->hname }}
                                        </li>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <input type="hidden" name="users_id" value="{!!Auth::user()->id !!}" />
                                    <input type="submit" name="save" id="save" class="btn btn-primary" value="Save" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script>
        $(document).ready(function(){

            var count = 1;

            hobby_field(count);

            function hobby_field(number)
            {
                html = '<div>';
                html += '<input type="text" name="hname[]" class="form-control" />';
                if(number > 1)
                {
                    html += '<button type="button" name="remove" id="" class="btn btn-danger remove">Remove</button></div>';
                    $('#hname').append(html);
                }
                else
                {
                    html += '<button type="button" name="add" id="add" class="btn btn-success">Add</button></div>';
                    $('#hname').append(html);
                }
            }

            $(document).on('click', '#add', function(){
                count++;
                hobby_field(count);
            });

            $(document).on('click', '.remove', function(){
                count--;
                $(this).closest("div").remove();
            });

            $('#hobbies_form').on('submit', function(event){
                event.preventDefault();
                $.ajax({
                    url:'{{ route("hobbies.update") }}',
                    method:'post',
                    data:$(this).serialize(),
                    dataType:'json',
                    beforeSend:function(){
                        $('#save').attr('disabled','disabled');
                    },
                    success:function(data)
                    {
                        if(data.error)
                        {
                            var error_html = '';
                            for(var count = 0; count < data.error.length; count++)
                            {
                                error_html += '<p>'+data.error[count]+'</p>';
                            }
                            $('#result').html('<div class="alert alert-danger">'+error_html+'</div>');
                        }
                        else
                        {
                            dynamic_field(1);
                            $('#result').html('<div class="alert alert-success">'+data.success+'</div>');
                        }
                        $('#save').attr('disabled', false);
                    }
                })
            });

        });
    </script>


@endsection
